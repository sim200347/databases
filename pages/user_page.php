<?php
require("connectdb.php");

$result = mysqli_query($connect, "SELECT *, pages.id AS page_id FROM pages JOIN users ON users.id = pages.user_id WHERE users.id=".$_GET['id']);

$title = "Записи пользователя";
$content = "";

if(!$result || mysqli_num_rows($result) == 0){
	$content = "В базе данных нет страниц.";
}
else{
    $content = "<ul>";
    while($page = mysqli_fetch_assoc($result)){
        $content .= "<li>
        <a href=\"page.php?id=".$page["page_id"]."\">
        ".$page["title"]."
        </a>
        |
        <a href=\"create_update.php?id=".$page["page_id"]."\">
        Редактировать
        </a>
        |
        <a href=\"delete.php?id=".$page["page_id"]."\">
        Удалить
        </a>
        |
        <a href=\"user_page.php?id=".$page["user_id"]."\">
        ".$page["fullname"]."
        </a>
        
        
        </a>
        </li>";   
    }
    $content .= "</ul>";
}


require("template.php");

?>