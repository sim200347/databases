<?php
include './device_status.php';
//--------------------------Настройки подключения к БД-----------------------
$db_host = 'std-mysql.ist.mospolytech.ru';
$db_user = 'std_1945_iot'; //имя пользователя совпадает с именем БД
$db_password = '200347ATs'; //пароль, указанный при создании БД
$database = 'std_1945_iot'; //имя БД, которое было указано при создании
$link = mysqli_connect($db_host, $db_user, $db_password, $database);
if (!$link) {
    die("Cannot connect DB");
}


//----------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------
function updateReleStatus($link, $id) {
    $query = "SELECT OUT_STATE FROM OUT_STATE_TABLE WHERE DEVICE_ID = '" . $id . "'";
    $result = mysqli_query($link, $query);
    $date_today = date("Y-m-d H:i:s"); //текущее время
    if (isset($_POST['button_on'])){
        $status = 1;
    } else {
        $status = 0;
    }
    if (mysqli_num_rows($result) == 1) { //Если в таблице есть данные для этого устройства - обновляем
        $query = "UPDATE OUT_STATE_TABLE SET OUT_STATE='" . $status . "', DATE_TIME='$date_today' WHERE DEVICE_ID = '" . $id . "'";
    } else { //Если данных для такого устройства нет - добавляем
        $query = "INSERT OUT_STATE_TABLE SET DEVICE_ID='" . $id . "', OUT_STATE='" . $status . "', DATE_TIME='$date_today'"; //Записать данные
    }
    $result = mysqli_query($link, $query);
}
//------Проверяем данные, полученные от пользователя---------------------
if (isset($_POST['add_device'])) {
    $device_name = $_POST['device_name'];
    try {
        $device_password = hash_pbkdf2("sha1", $_POST['device_password'], random_bytes(255), 32);
    } catch (Exception $e) {
        var_dump($e);
    }
    $query = "INSERT DEVICE_TABLE SET DEVICE_LOGIN='$device_name', DEVICE_PASSWORD='$device_password', NAME='$device_name'";
    $result = mysqli_query($link, $query);
}
if (isset($_POST['button_on'])) {
    $date_today = date("Y-m-d H:i:s");
    $id = $_POST['button_on'];
    $query = "UPDATE COMMAND_TABLE SET COMMAND='1', DATE_TIME='$date_today' WHERE DEVICE_ID = '$id'";
    $result = mysqli_query($link, $query);
    if (mysqli_affected_rows($link) != 1) //Если не смогли обновить - значит в таблице просто нет данных о команде для этого устройства
    { //вставляем в таблицу строчку с данными о команде для устройства
        $query = "INSERT COMMAND_TABLE SET DEVICE_ID='$id', COMMAND='1', DATE_TIME='$date_today'";
        $result = mysqli_query($link, $query);
    }
    updateReleStatus($link, $_POST['button_on']);
}

if (isset($_POST['button_off'])) {
    $date_today = date("Y-m-d H:i:s");
    $id = $_POST['button_off'];
    $query = "UPDATE COMMAND_TABLE SET COMMAND='0', DATE_TIME='$date_today' WHERE DEVICE_ID = '$id'";
    $result = mysqli_query($link, $query);
    if (mysqli_affected_rows($link) != 1) //Если не смогли обновить - значит в таблице просто нет данных о команде для этого устройства
    { //вставляем в таблицу строчку с данными о команде для устройства
        $query = "INSERT COMMAND_TABLE SET DEVICE_ID='$id', COMMAND='0', DATE_TIME='$date_today'";
        $result = mysqli_query($link, $query);
    }
    updateReleStatus($link, $_POST['button_off']);
}
//-----------------------------------------------------------------------

//-------Формируем интерфейс приложения для браузера---------------------
echo '
<!DOCTYPE HTML>
<html id="App_interface" lang="ru">
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>MyApp</title>
<script src="UpdateScript.js"> </script>
</head>
<body>';
$query = "SELECT * FROM DEVICE_TABLE";
$result = mysqli_query($link, $query);
while ($row = mysqli_fetch_assoc($result)) {
    $id = $row['DEVICE_ID'];
    $query = "SELECT * FROM OUT_STATE_TABLE WHERE DEVICE_ID = '$id'";
    $data = mysqli_query($link, $query);
    if (mysqli_num_rows($data) == 1) { //Если в БД есть данные о реле для этого устройства
        $Arr = mysqli_fetch_array($data);
        $out_state = $Arr['OUT_STATE'];
        $out_state_dt = $Arr['DATE_TIME'];
    } else { //Если в БД нет данных о реле для этого устройства
        $out_state = '?';
        $out_state_dt = '?';
    }
    $query = "SELECT * FROM TEMPERATURE_TABLE WHERE DEVICE_ID = '$id'";
    $data = mysqli_query($link, $query);
    if (mysqli_num_rows($data) == 1) { //Если в БД есть данные о температуре для этого устройства
        $Arr = mysqli_fetch_array($data);
        $temperature = $Arr['TEMPERATURE'];
        $temperature_dt = $Arr['DATE_TIME'];
    } else { //Если в БД нет данных о температуре для этого устройства
        $temperature = '?';
        $temperature_dt = '?';
    }
    echo '<table>
<tr>
<td width=100px> Устройство:
</td>
<td width=40px>' . $row['NAME'] . '
</td>
</tr>
</table>
<table border=1>
<tr>
<td width=100px> Tемпература
</td>
<td width=40px>' . $temperature . '
</td>
<td width=150px>' . $temperature_dt . '
</td>
</tr>
<tr>
<td width=100px> Реле
</td>
<td width=40px>' . $out_state . '
</td>
<td width=150px> ' . $out_state_dt . '
</td>
</tr>
</table>';
echo '<form>
<button formmethod=POST name=button_on value='.$row['DEVICE_ID'].'>Включить реле</button>
</form>
<form>
<button formmethod=POST name=button_off value='.$row['DEVICE_ID'].'>Выключить реле</button>
</form>';
}

echo '
<form>
<p>Добавьте новое устройство</p>
<label for="device_name">Название устройства</label>
<input id="device_name" type="text" name="device_name" placeholder="Название устройства">
<label for="device_name">Пароль</label>
<input id="device_name" type="password" name="device_password" placeholder="Пароль к устройству">
<button formmethod="post" name="add_device" value="1">Добавить</button>
</form>
</body>
</html>';
//----------------------------------------------------------------------
