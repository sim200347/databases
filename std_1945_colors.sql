-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Хост: std-mysql
-- Время создания: Окт 28 2022 г., 08:52
-- Версия сервера: 5.7.26-0ubuntu0.16.04.1
-- Версия PHP: 8.0.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `std_1945_colors`
--

DELIMITER $$
--
-- Процедуры
--
CREATE DEFINER=`std_1945_colors`@`%` PROCEDURE `clusterize_set` (`n` INT)   BEGIN
	DECLARE len INT;
	DECLARE i INT DEFAULT 1;
	DROP TABLE IF EXISTS iterations;
	CREATE TABLE iterations SELECT i as iteration;
	CALL kmeans(n);
	DROP TABLE IF EXISTS previous_centroids;
	CREATE TABLE previous_centroids SELECT * FROM centroids;
	UPDATE previous_centroids SET previous_centroids.r = -1, previous_centroids.g = -1,  previous_centroids.b = -1;
	SELECT * FROM previous_centroids;
	DROP TABLE IF EXISTS check_centroids;
	CREATE TABLE check_centroids SELECT * FROM centroids UNION SELECT * FROM previous_centroids;
	SELECT @len := COUNT(*) FROM check_centroids;
	WHILE @len > n DO
		CALL distances();
		CALL update_centroids();
		DROP TABLE IF EXISTS check_centroids;
		CREATE TABLE check_centroids SELECT * FROM centroids UNION SELECT * FROM previous_centroids;
		SELECT @len := COUNT(*) FROM check_centroids;
		UPDATE iterations SET iterations.iteration = i;
		SET i = i + 1;
	END WHILE;
	DROP TABLE IF EXISTS result;
	CREATE TABLE result SELECT * FROM colors JOIN distances ON distances.color = colors.id;
END$$

CREATE DEFINER=`std_1945_colors`@`%` PROCEDURE `distances` ()   BEGIN
	DROP TABLE IF EXISTS dist;
    CREATE TABLE dist SELECT centroids.id as centr, colors.id as color, SQRT(POWER(colors.r - centroids.r, 2) + POWER(colors.b  - centroids.b, 2) + POWER(colors.g - centroids.g, 2)) as dist FROM centroids, colors;
    DROP TABLE IF EXISTS distances;
	CREATE TABLE distances SELECT * FROM dist as dist1 WHERE (centr, color, dist) = (SELECT * FROM dist as dist2 WHERE dist2.color = dist1.color ORDER BY dist2.dist ASC LIMIT 1);
    DROP TABLE IF EXISTS dist;
END$$

CREATE DEFINER=`std_1945_colors`@`%` PROCEDURE `kmeans` (`k` INT)   BEGIN
	DROP TABLE IF EXISTS centroids;
	DROP TABLE IF EXISTS original_centroids;
	CREATE TABLE centroids SELECT r, g, b FROM colors ORDER BY RAND() LIMIT k;
	CREATE TABLE original_centroids SELECT * FROM centroids;
	ALTER TABLE centroids ADD COLUMN `id` INT AUTO_INCREMENT UNIQUE FIRST;
	ALTER TABLE original_centroids ADD COLUMN `id` INT AUTO_INCREMENT UNIQUE FIRST;
    SELECT * FROM original_centroids;
END$$

CREATE DEFINER=`std_1945_colors`@`%` PROCEDURE `update_centroids` ()   BEGIN
	DECLARE i INT;
  	SET i = 1;
  	DROP TABLE IF EXISTS previous_centroids;
  	CREATE TABLE previous_centroids SELECT * FROM centroids;
  	DROP TABLE IF EXISTS coordinates;
    DROP TABLE IF EXISTS centroids;
  	CREATE TABLE coordinates SELECT distances.centr, colors.r, colors.g, colors.b FROM colors JOIN distances ON colors.id = distances.color;
  	CREATE TABLE centroids SELECT coordinates.centr as id, CEIL(AVG(coordinates.r)) as r, CEIL(AVG(coordinates.g)) as g, CEIL(AVG(coordinates.b)) as b FROM coordinates GROUP BY coordinates.centr;
	DROP TABLE IF EXISTS coordinates;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Структура таблицы `centroids`
--

CREATE TABLE `centroids` (
  `id` int(11) NOT NULL DEFAULT '0',
  `r` bigint(13) DEFAULT NULL,
  `g` bigint(13) DEFAULT NULL,
  `b` bigint(13) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `centroids`
--

INSERT INTO `centroids` (`id`, `r`, `g`, `b`) VALUES
(1, 214, 89, 31),
(2, 51, 55, 218),
(3, 34, 249, 201),
(4, 89, 223, 51),
(5, 223, 64, 224);

-- --------------------------------------------------------

--
-- Структура таблицы `check_centroids`
--

CREATE TABLE `check_centroids` (
  `id` int(11) NOT NULL DEFAULT '0',
  `r` bigint(20) DEFAULT NULL,
  `g` bigint(20) DEFAULT NULL,
  `b` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `check_centroids`
--

INSERT INTO `check_centroids` (`id`, `r`, `g`, `b`) VALUES
(1, 214, 89, 31),
(2, 51, 55, 218),
(3, 34, 249, 201),
(4, 89, 223, 51),
(5, 223, 64, 224);

-- --------------------------------------------------------

--
-- Структура таблицы `colors`
--

CREATE TABLE `colors` (
  `id` int(11) NOT NULL,
  `r` int(11) DEFAULT NULL,
  `g` int(11) DEFAULT NULL,
  `b` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `colors`
--

INSERT INTO `colors` (`id`, `r`, `g`, `b`) VALUES
(1, 255, 173, 122),
(2, 194, 3, 0),
(3, 0, 0, 255),
(4, 26, 13, 245),
(5, 102, 0, 0),
(6, 97, 110, 196),
(7, 15, 227, 255),
(8, 0, 255, 255),
(9, 153, 51, 204),
(10, 0, 224, 0),
(11, 255, 229, 41),
(12, 0, 255, 0),
(13, 3, 255, 122),
(14, 128, 255, 0),
(15, 166, 0, 0),
(16, 255, 138, 128),
(17, 163, 20, 250),
(18, 0, 153, 0),
(19, 255, 0, 128),
(20, 255, 128, 77),
(21, 0, 191, 41),
(22, 10, 255, 255),
(23, 140, 0, 0),
(24, 255, 59, 33),
(25, 255, 46, 255),
(26, 64, 26, 255),
(27, 255, 120, 158),
(28, 77, 0, 0),
(29, 189, 255, 61),
(30, 31, 250, 163),
(31, 38, 255, 204),
(32, 255, 48, 255),
(33, 255, 10, 156),
(34, 143, 255, 66),
(35, 46, 255, 178),
(36, 0, 0, 0),
(37, 38, 255, 171),
(38, 184, 0, 0),
(39, 255, 125, 0),
(40, 255, 94, 255),
(41, 89, 222, 255),
(42, 255, 181, 41),
(43, 0, 255, 128),
(44, 115, 3, 235),
(45, 128, 128, 128),
(46, 217, 255, 79),
(47, 255, 133, 255),
(48, 255, 0, 255),
(49, 173, 0, 0),
(50, 0, 112, 145),
(51, 15, 117, 255),
(52, 255, 99, 33),
(53, 173, 92, 255),
(54, 110, 115, 255),
(55, 128, 0, 255),
(56, 140, 36, 255),
(57, 255, 0, 0),
(58, 150, 0, 168),
(59, 0, 128, 255),
(60, 255, 0, 222),
(61, 79, 255, 128),
(62, 97, 255, 224),
(63, 219, 148, 112),
(64, 224, 105, 255),
(65, 54, 31, 255),
(66, 255, 255, 255),
(67, 255, 255, 0),
(68, 255, 148, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `distances`
--

CREATE TABLE `distances` (
  `centr` int(11) NOT NULL DEFAULT '0',
  `color` int(11) NOT NULL DEFAULT '0',
  `dist` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `distances`
--

INSERT INTO `distances` (`centr`, `color`, `dist`) VALUES
(1, 1, 130.4530566909032),
(1, 2, 93.57884376289333),
(2, 3, 83.63611660042568),
(2, 4, 55.839054433254866),
(1, 5, 146.37622757811462),
(2, 6, 75),
(3, 7, 61.326992425847855),
(3, 8, 64.09368143584827),
(5, 9, 73.95268757793728),
(4, 10, 102.58167477673582),
(1, 11, 146.2224332994086),
(4, 12, 107.45231500530828),
(3, 13, 85.07643622061282),
(4, 14, 71.73562573784382),
(1, 15, 105.76388797694608),
(1, 16, 116.15076409563564),
(5, 17, 78.81624198095213),
(4, 18, 124.18534535121283),
(5, 19, 119.73303637676612),
(1, 20, 72.92461861401813),
(4, 21, 95.10520490488415),
(3, 22, 59.39696961966999),
(1, 23, 119.824872209404),
(1, 24, 50.84289527554464),
(5, 25, 48.05205510693585),
(2, 26, 48.774993593028796),
(5, 27, 92.28217596047462),
(1, 28, 166.28589837986863),
(4, 29, 105.47037498748168),
(3, 30, 38.13135192987524),
(3, 31, 7.810249675906654),
(5, 32, 47.3392015141785),
(5, 33, 92.5418824100742),
(4, 34, 64.53681120105021),
(3, 35, 26.627053911388696),
(2, 36, 230.54283766797008),
(3, 37, 30.854497241083024),
(1, 38, 98.90399385262458),
(1, 39, 62.75348595894893),
(5, 40, 53.71219600798314),
(3, 41, 81.67006795638167),
(1, 42, 101.21758740456127),
(3, 43, 80.75270893288967),
(2, 44, 84.19619943916709),
(4, 45, 128.35497652993436),
(4, 46, 134.87772240069893),
(5, 47, 82.13403679352427),
(5, 48, 77.9807668595276),
(1, 49, 102.77645644796283),
(2, 50, 105.73079021741964),
(2, 51, 80.67837380611982),
(1, 52, 42.24926034855522),
(5, 53, 65.1536645170477),
(2, 54, 91.92388155425118),
(2, 55, 101.6021653312566),
(5, 56, 92.91931984253867),
(1, 57, 102.77645644796283),
(5, 58, 112.07586716149021),
(2, 59, 96.43132271207318),
(5, 60, 71.58212067269312),
(4, 61, 83.98214095865859),
(3, 62, 67.3349834781297),
(1, 63, 100.33444074693395),
(5, 64, 51.41011573610781),
(2, 65, 44.204072210600685),
(5, 66, 196.12750954417385),
(1, 67, 173.77571752117728),
(1, 68, 78.24960063795854);

-- --------------------------------------------------------

--
-- Структура таблицы `iterations`
--

CREATE TABLE `iterations` (
  `iteration` bigint(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `iterations`
--

INSERT INTO `iterations` (`iteration`) VALUES
(5);

-- --------------------------------------------------------

--
-- Структура таблицы `original_centroids`
--

CREATE TABLE `original_centroids` (
  `id` int(11) NOT NULL,
  `r` int(11) DEFAULT NULL,
  `g` int(11) DEFAULT NULL,
  `b` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `original_centroids`
--

INSERT INTO `original_centroids` (`id`, `r`, `g`, `b`) VALUES
(1, 255, 148, 0),
(2, 0, 0, 255),
(3, 46, 255, 178),
(4, 79, 255, 128),
(5, 115, 3, 235);

-- --------------------------------------------------------

--
-- Структура таблицы `previous_centroids`
--

CREATE TABLE `previous_centroids` (
  `id` int(11) NOT NULL DEFAULT '0',
  `r` bigint(13) DEFAULT NULL,
  `g` bigint(13) DEFAULT NULL,
  `b` bigint(13) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `previous_centroids`
--

INSERT INTO `previous_centroids` (`id`, `r`, `g`, `b`) VALUES
(1, 214, 89, 31),
(2, 51, 55, 218),
(3, 34, 249, 201),
(4, 89, 223, 51),
(5, 223, 64, 224);

-- --------------------------------------------------------

--
-- Структура таблицы `result`
--

CREATE TABLE `result` (
  `id` int(11) NOT NULL DEFAULT '0',
  `r` int(11) DEFAULT NULL,
  `g` int(11) DEFAULT NULL,
  `b` int(11) DEFAULT NULL,
  `centr` int(11) NOT NULL DEFAULT '0',
  `color` int(11) NOT NULL DEFAULT '0',
  `dist` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `result`
--

INSERT INTO `result` (`id`, `r`, `g`, `b`, `centr`, `color`, `dist`) VALUES
(1, 255, 173, 122, 1, 1, 130.4530566909032),
(2, 194, 3, 0, 1, 2, 93.57884376289333),
(3, 0, 0, 255, 2, 3, 83.63611660042568),
(4, 26, 13, 245, 2, 4, 55.839054433254866),
(5, 102, 0, 0, 1, 5, 146.37622757811462),
(6, 97, 110, 196, 2, 6, 75),
(7, 15, 227, 255, 3, 7, 61.326992425847855),
(8, 0, 255, 255, 3, 8, 64.09368143584827),
(9, 153, 51, 204, 5, 9, 73.95268757793728),
(10, 0, 224, 0, 4, 10, 102.58167477673582),
(11, 255, 229, 41, 1, 11, 146.2224332994086),
(12, 0, 255, 0, 4, 12, 107.45231500530828),
(13, 3, 255, 122, 3, 13, 85.07643622061282),
(14, 128, 255, 0, 4, 14, 71.73562573784382),
(15, 166, 0, 0, 1, 15, 105.76388797694608),
(16, 255, 138, 128, 1, 16, 116.15076409563564),
(17, 163, 20, 250, 5, 17, 78.81624198095213),
(18, 0, 153, 0, 4, 18, 124.18534535121283),
(19, 255, 0, 128, 5, 19, 119.73303637676612),
(20, 255, 128, 77, 1, 20, 72.92461861401813),
(21, 0, 191, 41, 4, 21, 95.10520490488415),
(22, 10, 255, 255, 3, 22, 59.39696961966999),
(23, 140, 0, 0, 1, 23, 119.824872209404),
(24, 255, 59, 33, 1, 24, 50.84289527554464),
(25, 255, 46, 255, 5, 25, 48.05205510693585),
(26, 64, 26, 255, 2, 26, 48.774993593028796),
(27, 255, 120, 158, 5, 27, 92.28217596047462),
(28, 77, 0, 0, 1, 28, 166.28589837986863),
(29, 189, 255, 61, 4, 29, 105.47037498748168),
(30, 31, 250, 163, 3, 30, 38.13135192987524),
(31, 38, 255, 204, 3, 31, 7.810249675906654),
(32, 255, 48, 255, 5, 32, 47.3392015141785),
(33, 255, 10, 156, 5, 33, 92.5418824100742),
(34, 143, 255, 66, 4, 34, 64.53681120105021),
(35, 46, 255, 178, 3, 35, 26.627053911388696),
(36, 0, 0, 0, 2, 36, 230.54283766797008),
(37, 38, 255, 171, 3, 37, 30.854497241083024),
(38, 184, 0, 0, 1, 38, 98.90399385262458),
(39, 255, 125, 0, 1, 39, 62.75348595894893),
(40, 255, 94, 255, 5, 40, 53.71219600798314),
(41, 89, 222, 255, 3, 41, 81.67006795638167),
(42, 255, 181, 41, 1, 42, 101.21758740456127),
(43, 0, 255, 128, 3, 43, 80.75270893288967),
(44, 115, 3, 235, 2, 44, 84.19619943916709),
(45, 128, 128, 128, 4, 45, 128.35497652993436),
(46, 217, 255, 79, 4, 46, 134.87772240069893),
(47, 255, 133, 255, 5, 47, 82.13403679352427),
(48, 255, 0, 255, 5, 48, 77.9807668595276),
(49, 173, 0, 0, 1, 49, 102.77645644796283),
(50, 0, 112, 145, 2, 50, 105.73079021741964),
(51, 15, 117, 255, 2, 51, 80.67837380611982),
(52, 255, 99, 33, 1, 52, 42.24926034855522),
(53, 173, 92, 255, 5, 53, 65.1536645170477),
(54, 110, 115, 255, 2, 54, 91.92388155425118),
(55, 128, 0, 255, 2, 55, 101.6021653312566),
(56, 140, 36, 255, 5, 56, 92.91931984253867),
(57, 255, 0, 0, 1, 57, 102.77645644796283),
(58, 150, 0, 168, 5, 58, 112.07586716149021),
(59, 0, 128, 255, 2, 59, 96.43132271207318),
(60, 255, 0, 222, 5, 60, 71.58212067269312),
(61, 79, 255, 128, 4, 61, 83.98214095865859),
(62, 97, 255, 224, 3, 62, 67.3349834781297),
(63, 219, 148, 112, 1, 63, 100.33444074693395),
(64, 224, 105, 255, 5, 64, 51.41011573610781),
(65, 54, 31, 255, 2, 65, 44.204072210600685),
(66, 255, 255, 255, 5, 66, 196.12750954417385),
(67, 255, 255, 0, 1, 67, 173.77571752117728),
(68, 255, 148, 0, 1, 68, 78.24960063795854);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `colors`
--
ALTER TABLE `colors`
  ADD UNIQUE KEY `id` (`id`);

--
-- Индексы таблицы `original_centroids`
--
ALTER TABLE `original_centroids`
  ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `colors`
--
ALTER TABLE `colors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT для таблицы `original_centroids`
--
ALTER TABLE `original_centroids`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
